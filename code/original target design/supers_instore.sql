


DECLARE min_ref_dt DATE;
DECLARE max_ref_dt DATE;
--DECLARE num_CRNs INTEGER; 

-- replace with relevant timeframe
SET min_ref_dt = '2020-10-30';
SET max_ref_dt = '2021-10-30';
-- replace with number of CRNs to sample
--SET num_CRNs = 5000000;

drop TABLE if exists akelly.temp_base_cust_2020_to_2021_instore; 
CREATE OR REPLACE TABLE akelly.temp_base_cust_2020_to_2021_instore AS
SELECT 
     crn
    ,ref_dt
    ,case when supers_tot_spend>=0 and supers_tot_spend<=500 then '01. 0 to 500'
        when supers_tot_spend>500 and supers_tot_spend<=1000 then '02. 500 to 1000'
        when supers_tot_spend>1000 and supers_tot_spend<=2000 then '03. 1000 to 2000'
        when supers_tot_spend>2000 and supers_tot_spend<=3000 then '04. 2000 to 3000'
        when supers_tot_spend>3000 and supers_tot_spend<=4000 then '05. 3000 to 4000'
        when supers_tot_spend>4000 and supers_tot_spend<=5000 then '06. 4000 to 5000'
        when supers_tot_spend>5000 and supers_tot_spend<=6000 then '07. 5000 to 6000'
        when supers_tot_spend>6000 and supers_tot_spend<=7000 then '08. 6000 to 7000'
        when supers_tot_spend>7000 and supers_tot_spend<=8000 then '09. 7000 to 8000'
        when supers_tot_spend>8000 and supers_tot_spend<=9000 then '10. 8000 to 9000'
        when supers_tot_spend>9000 and supers_tot_spend<=10000 then '11. 9000 to 10000'
        when supers_tot_spend>10000  then '12. 10000+'
        end as spend_range
FROM gcp-wow-rwds-ai-cfv-dev.supers.supers_target
WHERE ref_dt >= min_ref_dt
        AND ref_dt <= max_ref_dt
        AND supers_tot_spend_binary=1
        AND supers_tot_spend>30
        AND supers_tot_spend<20000
GROUP BY 
     1,2,3


CREATE OR REPLACE TABLE akelly.sample_base_cust_full_model_2020_to_2021_instore AS
with 
CRN_REF_DATES AS
(
    SELECT
         bse.crn
        ,bse.ref_dt
        ,bse.spend_range
        ,RANK() OVER (PARTITION BY bse.crn ORDER BY spend_range desc) RankNum,
        ROW_NUMBER() OVER (PARTITION BY bse.crn ) as RankNum2
    FROM
        gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021_instore bse
        #where bse.crn not in (select crn from gcp-wow-rwds-ai-cfv-dev.akelly.sample_base_cust_full_model_2020_to_2021_ecom)
)

SELECT 
     crn
    ,ref_dt,spend_range,d.RankNum,d.seqnum,d.RankNum2
FROM  (
        select d.spend_range,d.crn,d.ref_dt,d.RankNum,d.RankNum2,
              row_number() over (partition by spend_range ORDER BY spend_range desc) as seqnum
      from CRN_REF_DATES d WHERE d.RankNum = 1 and d.RankNum2=1
      order by rand()
     ) d 
WHERE seqnum <=300000;



drop table if exists akelly.instore_base_2020_to_2021 ; 
create or replace table akelly.instore_base_2020_to_2021 as
select
    b.crn
    ,b.ref_dt
    ,a52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,a26.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,a12.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,a8.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,a4.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,a2.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ls.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cf52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cf26.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cf12.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cf8.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cf4.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cf2.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bw52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bw26.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bw12.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bw8.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bw4.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bw2.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bwe.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bws52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bws26.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bws12.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bws8.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bws4.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bws2.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,bwse.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ec52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ec12.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ec8.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ec4.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ec2.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,s52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,s26.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,s12.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,s8.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,s4.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,s2.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ste.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,cd.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ds.* EXCEPT (crn,ref_dt)
    ,ps.* EXCEPT (crn,ref_dt)
    ,pc.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ecom.* EXCEPT (crn,ref_dt
        ,onln_last_survey_EASE_OF_COLLECTING_REASON_OTHER
        ,onln_last_survey_CONDITION_OF_PACKAGED_COMMENT
        ,onln_last_survey_EASE_OF_CHECKING_OUT_AND_PAYING_FOR_ORDER_COMMENT
        ,onln_last_survey_TIME_ORDER_DELIVERED_COMMENT
        ,onln_last_survey_COLLECTION_TIMES_AVAILABLE_COMMENT
        ,onln_last_survey_DEALS_AND_SPECIALS_ON_WEBSITE_COMMENT
        ,onln_last_survey_PRICE_OF_PRODUCTS_ON_WEBSITE_COMMENT
        ,onln_last_survey_FRIENDLINESS_OF_DELIVERY_DRIVER_COMMENT
        ,onln_last_survey_EASE_OF_NAVIGATING_WEBSITE_COMMENT
        ,onln_last_survey_COMPLETENESS_ACCURACY_REASON_OTHER
        ,onln_last_survey_QUALITY_AND_FRESHNESS_OF_FRESH_FOOD_COMMENT
        ,onln_last_survey_WEBSITE_BEING_FAST_AND_RELIABLE_COMMENT
        ,onln_last_survey_WEBSITE_CONTENT_BEING_RELEVANT_AND_HELPFUL_COMMENT
        ,onln_last_survey_RANGE_OF_PRODUCTS_COMMENT
        ,onln_last_survey_RECEIVED_SUBSTITUTION
        ,onln_last_survey_OVERALL_SATISFACTION_FEEDBACK)
    ,et.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,it.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ecom26.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ecom52.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
    ,ecome.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
-- replace following with sample table
from akelly.sample_base_cust_full_model_2020_to_2021_instore b
left join ecom.ecom_target et on et.crn = b.crn and et.ref_dt = b.ref_dt
left join supers.supers_target it on it.crn = b.crn and it.ref_dt = b.ref_dt
left join feature_build.cfv_app_online_w12 a12 on a12.crn = b.crn and a12.ref_dt = b.ref_dt
left join feature_build.cfv_app_online_w2 a2 on a2.crn = b.crn and a2.ref_dt = b.ref_dt
left join feature_build.cfv_app_online_w26 a26 on a26.crn = b.crn and a26.ref_dt = b.ref_dt
left join feature_build.cfv_app_online_w4 a4 on a4.crn = b.crn and a4.ref_dt = b.ref_dt
left join feature_build.cfv_app_online_w52 a52 on a52.crn = b.crn and a52.ref_dt = b.ref_dt
left join feature_build.cfv_app_online_w8 a8 on a8.crn = b.crn and a8.ref_dt = b.ref_dt
left join ecom.ecom_lifecycle_segm ls on ls.crn = b.crn and ls.ref_dt = b.ref_dt
left join feature_build.camp_flags_w52 cf52 on cf52.crn = b.crn and cf52.ref_dt = b.ref_dt
left join feature_build.camp_flags_w26 cf26 on cf26.crn = b.crn and cf26.ref_dt = b.ref_dt
left join feature_build.camp_flags_w12 cf12 on cf12.crn = b.crn and cf12.ref_dt = b.ref_dt
left join feature_build.camp_flags_w8 cf8 on cf8.crn = b.crn and cf8.ref_dt = b.ref_dt
left join feature_build.camp_flags_w4 cf4 on cf4.crn = b.crn and cf4.ref_dt = b.ref_dt
left join feature_build.camp_flags_w2 cf2 on cf2.crn = b.crn and cf2.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_w52 bw52 on bw52.crn = b.crn and bw52.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_w26 bw26 on bw26.crn = b.crn and bw26.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_w12 bw12 on bw12.crn = b.crn and bw12.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_w8 bw8 on bw8.crn = b.crn and bw8.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_w4 bw4 on bw4.crn = b.crn and bw4.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_w2 bw2 on bw2.crn = b.crn and bw2.ref_dt = b.ref_dt
left join feature_build.cfv_bigw_txn_extn bwe on bwe.crn = b.crn and bwe.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_w52 bws52 on bws52.crn = b.crn and bws52.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_w26 bws26 on bws26.crn = b.crn and bws26.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_w12 bws12 on bws12.crn = b.crn and bws12.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_w8 bws8 on bws8.crn = b.crn and bws8.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_w4 bws4 on bws4.crn = b.crn and bws4.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_w2 bws2 on bws2.crn = b.crn and bws2.ref_dt = b.ref_dt
left join feature_build.cfv_bws_txn_extn bwse on bwse.crn = b.crn and bwse.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_onln_spend_catg_w52 ec52 on ec52.crn = b.crn and ec52.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_onln_spend_catg_w12 ec12 on ec12.crn = b.crn and ec12.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_onln_spend_catg_w8 ec8 on ec8.crn = b.crn and ec8.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_onln_spend_catg_w4 ec4 on ec4.crn = b.crn and ec4.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_onln_spend_catg_w2 ec2 on ec2.crn = b.crn and ec2.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_w52 s52 on s52.crn = b.crn and s52.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_w26 s26 on s26.crn = b.crn and s26.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_w12 s12 on s12.crn = b.crn and s12.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_w8 s8 on s8.crn = b.crn and s8.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_w4 s4 on s4.crn = b.crn and s4.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_w2 s2 on s2.crn = b.crn and s2.ref_dt = b.ref_dt
left join feature_build.cfv_supers_txn_extn ste on ste.crn = b.crn and ste.ref_dt = b.ref_dt
left join feature_build.cust_detail cd on cd.crn = b.crn and cd.ref_dt = b.ref_dt
left join feature_build.du_subscription_features ds on ds.crn = b.crn and ds.ref_dt = b.ref_dt
left join feature_build.preferred_store_features ps on ps.crn = b.crn and ps.ref_dt = b.ref_dt
left join feature_build.prx_and_comp pc on pc.crn = b.crn and pc.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_txn_w26 ecom26 on ecom26.crn = b.crn and ecom26.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_txn_w52 ecom52 on ecom52.crn = b.crn and ecom52.ref_dt = b.ref_dt
left join feature_build.cfv_ecom_txn_extn ecome on  ecome.crn = b.crn and  ecome.ref_dt = b.ref_dt
left join wx-bq-poc.ecom.ecom_onln_features ecom on ecom.crn = b.crn and ecom.ref_dt = b.ref_dt
where it.supers_tot_spend is not null and it.supers_tot_spend>=30 and it.supers_tot_spend<=20000
;



CREATE TABLE akelly.instore_base_2020_to_2021_with_kimchi as select a.*
,b.* EXCEPT (crn,ref_ts) from akelly.instore_base_2020_to_2021 as a left join 
gcp-wow-rwds-ai-mlt-evs-dev.kimchi_digital_features.crn__wow_ecom 
on a.crn=b.crn and a.ref_dt=DATE(b.ref_ts);



#PENDING BACKDATE
CREATE TABLE akelly.instore_base_2020_to_2021_with_kimchi_rewards as select a.*
,b.* EXCEPT (crn,ref_ts) from akelly.instore_base_2020_to_2021 as a left join 
gcp-wow-rwds-ai-mlt-evs-dev.kimchi_digital_features.crn__rapp 
on a.crn=b.crn and a.ref_dt=DATE(b.ref_ts);




CREATE TABLE akelly.instore_base_2020_to_2021_with_kimchi_with_cust AS 
SELECT a.*,

b.TitleDescription, b.gender, b.Age,  b.LeisureActivityCode
FROM akelly.instore_base_2020_to_2021_with_kimchi AS a left join
(SELECT 
 b.CustomerRegistrationNumber,
b.TitleDescription, 
case when UPPER(b.Gender)='F' then 'F'
when UPPER(b.Gender)='M' then 'M' else 'U' end as gender,  b.Age,  b.LeisureActivityCode,
RANK() OVER (PARTITION BY CustomerRegistrationNumber ORDER BY load_dttm DESC) dest_rank from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.dim_customer_v as b
) b 
on 
a.crn=b.CustomerRegistrationNumber 
where  dest_rank=1;



drop table if exists akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_class ; 
CREATE TABLE akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_class AS 
SELECT a.*,b.ncluster from akelly.instore_base_2020_to_2021_with_kimchi_with_cust  as a left join 
gcp-wow-rwds-ai-beh-seg-prod.segmentations.super  as b on a.crn=b.crn
where extract( YEAR from b.pw_end_Date) =2020 and extract( MONTH from b.pw_end_Date) = 11 ;






CREATE OR REPLACE TABLE akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer AS 

WITH OFFERS_8wk AS (
SELECT a.crn, 
    COUNT(CASE WHEN c.udo_tealium_event IN ('screen','welcome_screen','splash_screen','discoverhome_screen','favourites_screen','activityfeed_screen','processing_screen','extras_screen','foryou_screen','yourspecials_screen','fuelvouchers_screen','receipt_screen','perksdetails_screen','receipt_preferences_screen','points_screen','everydaypay_screen','discoversearch_screen','rewards_activityfeed_screen','partneroverview_screen','offerdetails_screen','scanqrcode_screen','promotiondetails_screen','rewardspartnersearch_screen','rewardsaccount_screen','addbankcard_screen','partnerdetails_screen','rewardscard_screen','carddetail_screen','updatecard_screen','push_notifications_preferences_screen','readytopaynow_screen','redemptionsettings_screen','preference_details_screen','readytopaynowviadeeplink_screen','paymentsuccess_screen'
    ) THEN 1 ELSE NULL END) AS f__rapp_cnt_screen_views_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'splash_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_splash_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'welcome_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_welcome_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'discoverhome_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_discoverhome_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'favourites_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_favourites_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'activityfeed_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_activityfeed_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'processing_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_processing_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'extras_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_extras_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'foryou_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_foryou_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'yourspecials_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_yourspecials_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'fuelvouchers_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_fuelvouchers_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'receipt_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_receipt_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'perksdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_perksdetails_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'receipt_preferences_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_receipt_preferences_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'points_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_points_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'everydaypay_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_everydaypay_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'discoversearch_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_discoversearch_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewards_activityfeed_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewards_activityfeed_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'partneroverview_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_partneroverview_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'offerdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_offerdetails_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'scanqrcode_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_scanqrcode_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'promotiondetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_promotiondetails_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardspartnersearch_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardspartnersearch_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardsaccount_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardsaccount_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'addbankcard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_addbankcard_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'partnerdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_partnerdetails_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardscard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardscard_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'carddetail_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_carddetail_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'updatecard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_updatecard_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'push_notifications_preferences_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_push_notifications_preferences_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'readytopaynow_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_readytopaynow_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'redemptionsettings_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_redemptionsettings_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'preference_details_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_preference_details_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'readytopaynowviadeeplink_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_readytopaynowviadeeplink_screen_8w,
    COUNT(CASE c.udo_tealium_event WHEN 'paymentsuccess_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_paymentsuccess_screen_8w
FROM akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_class  AS a 
left join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on a.crn=b.crn
left join `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as c on
c.udo_user_profile_crn_hash=b.crn_enc
WHERE DATE(post_time) >= DATE_SUB(a.ref_dt, INTERVAL 8 WEEK)
 AND DATE(post_time) <  DATE(a.ref_dt)
group by 1),

OFFERS_4wk AS (
SELECT a.crn, 
    COUNT(CASE WHEN c.udo_tealium_event IN ('screen','welcome_screen','splash_screen','discoverhome_screen','favourites_screen','activityfeed_screen','processing_screen','extras_screen','foryou_screen','yourspecials_screen','fuelvouchers_screen','receipt_screen','perksdetails_screen','receipt_preferences_screen','points_screen','everydaypay_screen','discoversearch_screen','rewards_activityfeed_screen','partneroverview_screen','offerdetails_screen','scanqrcode_screen','promotiondetails_screen','rewardspartnersearch_screen','rewardsaccount_screen','addbankcard_screen','partnerdetails_screen','rewardscard_screen','carddetail_screen','updatecard_screen','push_notifications_preferences_screen','readytopaynow_screen','redemptionsettings_screen','preference_details_screen','readytopaynowviadeeplink_screen','paymentsuccess_screen'
    ) THEN 1 ELSE NULL END) AS f__rapp_cnt_screen_views_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'splash_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_splash_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'welcome_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_welcome_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'discoverhome_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_discoverhome_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'favourites_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_favourites_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'activityfeed_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_activityfeed_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'processing_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_processing_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'extras_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_extras_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'foryou_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_foryou_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'yourspecials_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_yourspecials_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'fuelvouchers_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_fuelvouchers_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'receipt_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_receipt_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'perksdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_perksdetails_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'receipt_preferences_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_receipt_preferences_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'points_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_points_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'everydaypay_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_everydaypay_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'discoversearch_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_discoversearch_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewards_activityfeed_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewards_activityfeed_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'partneroverview_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_partneroverview_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'offerdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_offerdetails_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'scanqrcode_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_scanqrcode_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'promotiondetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_promotiondetails_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardspartnersearch_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardspartnersearch_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardsaccount_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardsaccount_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'addbankcard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_addbankcard_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'partnerdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_partnerdetails_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardscard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardscard_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'carddetail_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_carddetail_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'updatecard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_updatecard_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'push_notifications_preferences_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_push_notifications_preferences_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'readytopaynow_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_readytopaynow_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'redemptionsettings_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_redemptionsettings_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'preference_details_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_preference_details_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'readytopaynowviadeeplink_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_readytopaynowviadeeplink_screen_4w,
    COUNT(CASE c.udo_tealium_event WHEN 'paymentsuccess_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_paymentsuccess_screen_4w
FROM akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_class  AS a 
left join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on a.crn=b.crn
left join `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as c on
c.udo_user_profile_crn_hash=b.crn_enc
WHERE DATE(post_time) >= DATE_SUB(a.ref_dt, INTERVAL 4 WEEK)
AND DATE(post_time) <  DATE(a.ref_dt)
group by 1),

OFFERS_2wk AS (
SELECT a.crn, 
    COUNT(CASE WHEN c.udo_tealium_event IN ('screen','welcome_screen','splash_screen','discoverhome_screen','favourites_screen','activityfeed_screen','processing_screen','extras_screen','foryou_screen','yourspecials_screen','fuelvouchers_screen','receipt_screen','perksdetails_screen','receipt_preferences_screen','points_screen','everydaypay_screen','discoversearch_screen','rewards_activityfeed_screen','partneroverview_screen','offerdetails_screen','scanqrcode_screen','promotiondetails_screen','rewardspartnersearch_screen','rewardsaccount_screen','addbankcard_screen','partnerdetails_screen','rewardscard_screen','carddetail_screen','updatecard_screen','push_notifications_preferences_screen','readytopaynow_screen','redemptionsettings_screen','preference_details_screen','readytopaynowviadeeplink_screen','paymentsuccess_screen'
    ) THEN 1 ELSE NULL END) AS f__rapp_cnt_screen_views_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'splash_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_splash_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'welcome_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_welcome_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'discoverhome_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_discoverhome_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'favourites_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_favourites_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'activityfeed_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_activityfeed_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'processing_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_processing_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'extras_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_extras_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'foryou_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_foryou_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'yourspecials_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_yourspecials_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'fuelvouchers_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_fuelvouchers_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'receipt_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_receipt_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'perksdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_perksdetails_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'receipt_preferences_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_receipt_preferences_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'points_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_points_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'everydaypay_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_everydaypay_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'discoversearch_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_discoversearch_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewards_activityfeed_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewards_activityfeed_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'partneroverview_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_partneroverview_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'offerdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_offerdetails_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'scanqrcode_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_scanqrcode_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'promotiondetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_promotiondetails_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardspartnersearch_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardspartnersearch_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardsaccount_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardsaccount_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'addbankcard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_addbankcard_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'partnerdetails_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_partnerdetails_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'rewardscard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_rewardscard_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'carddetail_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_carddetail_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'updatecard_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_updatecard_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'push_notifications_preferences_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_push_notifications_preferences_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'readytopaynow_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_readytopaynow_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'redemptionsettings_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_redemptionsettings_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'preference_details_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_preference_details_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'readytopaynowviadeeplink_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_readytopaynowviadeeplink_screen_2w,
    COUNT(CASE c.udo_tealium_event WHEN 'paymentsuccess_screen' THEN 1 ELSE NULL END) AS f__rapp_cnt_paymentsuccess_screen_2w

FROM akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_class  AS a 
left join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on a.crn=b.crn
left join `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as c on
c.udo_user_profile_crn_hash=b.crn_enc
WHERE DATE(post_time) >= DATE_SUB(a.ref_dt, INTERVAL 2 WEEK)
AND DATE(post_time) <  DATE(a.ref_dt)
group by 1)

SELECT a.*, b.* EXCEPT(crn), c.* EXCEPT(crn) from akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_class  as a left join OFFERS_2wk AS b 
on a.crn=b.crn left join OFFERS_4wk AS c on a.crn=c.crn left join OFFERS_8wk as d on a.crn=d.crn; 












